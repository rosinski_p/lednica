<?php
session_start();
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
	if (isset($_POST['user']))
	{
		$wszystko_ok=true; //założenie walidacji
		//username
		$user=$_POST['user'];
		//dlugosc lancucha
		if((strlen($user)<3) || (strlen($user)>20))
		{
			$wszystko_ok=false;
			$_SESSION['e_user']="Nazwa użytkownika musi składać się z 3 do 20 znaków!";
		}
		//cyfry
		if (ctype_alnum($user)==false)
		{
			$wszystko_ok=false;
			$_SESSION['e_user']="Nazwa użytkownika może się składać tylko z cyfr i liter";
		}
		//email
		$email=$_POST['email'];
		$emailB=filter_var($email, FILTER_SANITIZE_EMAIL);
		
		if((filter_var($emailB, FILTER_VALIDATE_EMAIL)==FALSE ||($emailB!=$email)))
		{
			$wszystko_ok=false;
			$_SESSION['e_email']="Podaj poprawny adres e-mail!";
		}
		
		//haslo
		
		$password1=$_POST['password1'];
		$password2=$_POST['password2'];
		
		if((strlen($password1)<8) || (strlen($password1)>20))
		{
			$wszystko_ok=false;
			$_SESSION['e_password']="Hasło powinno zawierać od 8 do 20 znaków!";
		}
		if ($password1!=$password2)
		{
			$wszystko_ok=false;
			$_SESSION['e_password']="Hasła nie są zgodne!";
			
		}
		$lastlogin=date("Y-n-d");
		
		//typ
		
		$typ=$_POST['typ'];
		
		$password_hash=password_hash($password1, PASSWORD_DEFAULT);
		
		
		require_once "connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		
		try 
		{
			$polaczenie = new mysqli($host,$user,$password,$database);
			if($polaczenie->connect_errno!=0)
		{
			throw new Exception(mysqli_connect_errno());
		}
		else
		{
			//Sprawdzanie e-maila
			$rezultat=$polaczenie->query("SELECT iduser FROM users WHERE email='$emailB'");
			
			if(!$rezultat) 
			{
				throw new Exception($polaczenie->error);
			}
			
			$ile_user=$rezultat->num_rows;
			
			if($ile_user>0)
			{
				$wszystko_ok=false;
				$_SESSION['e_email']="Podany adres e-mail już istnieje w bazie!";	
			}
			//sprawdzanie usera
			
			$rezultat=$polaczenie->query("SELECT iduser FROM users WHERE user='$user'");
			
			if(!$rezultat) 
			{
				throw new Exception($polaczenie->error);
			}
			
			$ile_maili=$rezultat->num_rows;
			
			if($ile_maili>0)
			{
				$wszystko_ok=false;
				$_SESSION['e_user']="Podany użytkownik już istnieje w bazie!";	
			}
			
			
			if($wszystko_ok==true)
			{
				if($polaczenie->query("INSERT INTO users VALUES (NULL, '$user','$password_hash','$email','$typ','$lastlogin')"))
				{
					$_SESSION['udanarejestracja']=true;
					header('Location: firstlogin.php');
					$_SESSION['zalogowany']=false;
					
				}
				else
				{
					throw new Exception($polaczenie->error);
				}
			}
			

			$polaczenie->close();
		}
			
		}
		catch (Exception $e)
		{
			echo 'Błąd serwera!';
			echo '<br/>Dev Info: '.$e;//zakomciać! <3 <3 <3
		}
		
		
	
	}
	
	
?>


<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>Lednicki system sprzedażowo-magazynowy</title>
	<meta name="description" content="Opis strony"/>
	
	<meta http-equiv="X-UA-Compatyible" content ="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="style.css" media="all">
</head>

<body>

	<div id="container">
		<div id="header">
		<h1>Lednicki system magazynowy</h1>
	
		</div>
		<div id="formularz">
		<h3>Dodaj użytkownika</h3>
		<form  method="post">
		Nazwa użytkownika: <input type="text" name="user" /><br /> 
		<?php 
		if (isset($_SESSION['e_user']))
		{
			echo '<div class="error">'.$_SESSION['e_user'].'</div>';
			unset($_SESSION['e_user']);
		}
		?>
		E-mail: <input type="text" name="email"  /><br />
		<?php 
		if (isset($_SESSION['e_email']))
		{
			echo '<div class="error">'.$_SESSION['e_email'].'</div>';
			unset($_SESSION['e_email']);
		}
		?>
		Hasło: <input type="password" name="password1"/><br />
		Powtórz hasło: <input type="password" name="password2"/><br />
		<?php
		if (isset($_SESSION['e_password']))
		{
			echo '<div class="error">'.$_SESSION['e_password'].'</div>';
			unset($_SESSION['e_password']);
		}
		?>
		Typ: <select name="typ">
		<option>Administrator</option>
		<option>Moderator</option>
		<option>Wizytor</option>
		</select>
		<br />
		<br /><button type="submit">Zarejestruj</button>
		<br /><br /><button type="reset">Wyczyść dane</button>
		</form>
		<p>
		<p>
		</div>
		
		<div id="footer">
		<h3>Prawa autorskie</h3>
		
		<div id="powrot">[<a href="main.php">Strona Główna</a>]</div>
		<p>
		[<a href="logout.php">Wyloguj się!</a>]
		</p>
		</div>
		
		
	</div>
	
</body>
</html>