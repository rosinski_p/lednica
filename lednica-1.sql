-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 24 Maj 2016, 10:32
-- Wersja serwera: 10.1.10-MariaDB
-- Wersja PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lednica`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `connect`
--

CREATE TABLE `connect` (
  `idconnect` int(11) NOT NULL,
  `idpaleta` int(11) NOT NULL,
  `idksiazka` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `data_mod` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ksiazka`
--

CREATE TABLE `ksiazka` (
  `idksiazka` int(11) NOT NULL,
  `typ` text COLLATE utf8_polish_ci NOT NULL,
  `isbn` bigint(20) NOT NULL,
  `tytul` text COLLATE utf8_polish_ci NOT NULL,
  `autor` text COLLATE utf8_polish_ci NOT NULL,
  `rok` year(4) NOT NULL,
  `wydawnictwo` text COLLATE utf8_polish_ci NOT NULL,
  `zrodlo` text COLLATE utf8_polish_ci NOT NULL,
  `waga` float NOT NULL,
  `cena` float NOT NULL,
  `ilosc` int(11) NOT NULL,
  `data_modyfikacji` date NOT NULL,
  `tworca` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ksiazka`
--

INSERT INTO `ksiazka` (`idksiazka`, `typ`, `isbn`, `tytul`, `autor`, `rok`, `wydawnictwo`, `zrodlo`, `waga`, `cena`, `ilosc`, `data_modyfikacji`, `tworca`) VALUES
(2, '1', 9310101010101, 'Tytuł książki', 'Ktoś', 1994, 'Znak', 'Darowizna', 0.23, 29.99, 1200, '2016-05-17', ''),
(3, '$typ', 0, '$tytul', '$autor', 0000, '$wydawnictwo', '$zrodlo', 0, 0, 0, '0000-00-00', '$tworca'),
(4, 'GadÅ¼et', 1111111111, 'Tropy', 'Wielu AutorÃ³w', 1975, 'Stowarzyszenie Lednica2000', 'WÅ‚asne', 0, 25, 1000, '2016-05-19', 'Admin'),
(5, 'GadÅ¼et', 12345678910, 'Tropy Lednickie', 'Wielu AutorÃ³w', 1990, 'Stowarzyszenie Lednica2000', 'WÅ‚asne', 0, 25, 1000, '2016-05-19', 'Admin'),
(6, 'GadÅ¼et', 99999999991, 'Tropy1', 'Wielu AutorÃ³w', 1990, 'Lednica2000', 'WÅ‚asne', 0, 25, 1000, '2016-05-19', 'Admin'),
(7, 'GadÅ¼et', 11111111112, 'Trop2', 'Autor', 1990, 'Lednica2000', 'WÅ‚asne', 0, 25, 1000, '2016-05-19', 'sq6pid'),
(8, 'GadÅ¼et', 123456789102, 'Å»Ã³Å‚Ä‡', 'Å¹Ä†Ã“Å', 1975, 'Ä…Ä™Ä‡', 'WÅ‚asne', 0, 25, 1000, '2016-05-19', 'sq6pid'),
(9, 'Gadżet', 123456789103, 'Żółć', 'ŹĆÓŁ', 2000, 'ąęć', 'Własne', 0, 25, 1000, '2016-05-19', 'sq6pid'),
(10, 'Książka', 12345678914, 'Żółć1', 'ŹĆÓŁ', 1975, 'Lednica2000', 'Własne', 0, 25, 1000, '2016-05-19', 'sq6pid'),
(11, 'Gadżet', 1, 'Krzyż Amen', 'Lednica2000', 2016, 'Stowarzyszenie Lednica2000', 'Własne', 0, 2, 80000, '2016-05-19', 'Admin'),
(12, 'Testowa', 99, '', '', 0000, '', '', 0, 0, 0, '0000-00-00', ''),
(13, 'Gadżet', 103, 'Butelka', 'Lednica2000', 2012, 'Lednica2000', 'Własne', 0, 10, 40000, '2016-05-19', 'Admin');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `paleta`
--

CREATE TABLE `paleta` (
  `idpaleta` int(11) NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL,
  `place` text COLLATE utf8_polish_ci NOT NULL,
  `destination` text COLLATE utf8_polish_ci NOT NULL,
  `state` text COLLATE utf8_polish_ci NOT NULL,
  `out_date` date NOT NULL,
  `back_date` date NOT NULL,
  `stocktaking` tinyint(1) NOT NULL,
  `place_change` tinyint(1) NOT NULL,
  `place_reserved` tinyint(1) NOT NULL,
  `interchange` tinyint(1) NOT NULL,
  `weight` float NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `paleta`
--

INSERT INTO `paleta` (`idpaleta`, `name`, `place`, `destination`, `state`, `out_date`, `back_date`, `stocktaking`, `place_change`, `place_reserved`, `interchange`, `weight`, `price`) VALUES
(1, '$name', '$place', '$destination', '$state', '0000-00-00', '0000-00-00', 0, 0, 0, 0, 0, 0),
(2, 'KsiÄ…Å¼ki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(3, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(4, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(5, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(6, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(7, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(8, 'Książki_mix', 'D3', 'Ryba', 'Lokalna', '2016-05-19', '2016-05-20', 1, 1, 1, 1, 0, 0),
(9, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(10, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(11, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(12, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(13, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(14, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(15, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(16, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(17, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(18, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(19, '', '', '', '', '0000-00-00', '0000-00-00', 1, 1, 1, 1, 0, 0),
(20, 'Książki_mix', 'D5', 'Magazyn', 'Lokalna', '2016-05-20', '2016-05-21', 1, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `place`
--

CREATE TABLE `place` (
  `idplace` int(11) NOT NULL,
  `place` text COLLATE utf8_polish_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `add_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `place`
--

INSERT INTO `place` (`idplace`, `place`, `priority`, `add_date`) VALUES
(1, 'Ryba', 1, '2016-05-19'),
(2, 'Magazyn', 3, '2016-05-19'),
(3, '$place', 0, '0000-00-00'),
(4, '', 0, '0000-00-00'),
(5, '', 0, '0000-00-00'),
(6, '', 0, '2016-05-20'),
(7, '', 0, '2016-05-20');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `user` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `level` text COLLATE utf8_polish_ci NOT NULL,
  `lastlogin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`iduser`, `user`, `password`, `email`, `level`, `lastlogin`) VALUES
(1, 'Admin', '$2y$10$LGRPn4FLPgWMYswnzNtOJO73cDsyAeASYWa8POHJrf3LstB6vgpsO', 'dziedziul@dekafot.pl', '0', '2016-05-20'),
(3, 'Guest', '$2y$10$bYNu3F635h.Ax984j6yD.OEbuktGnyKdHuAbVUDFeKnry8T1MGhMm', 'guest@lednica', '2', '2016-05-01'),
(6, 'sq6pid', '$2y$10$lnQQ.ht/2FjbqfrgY9eUX.Y3cyTsPBdIJn5hhwYSAEQBtgJeFkdoO', 'piotrdziedziul@gmail.com', 'Administrator', '2016-05-19');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `connect`
--
ALTER TABLE `connect`
  ADD PRIMARY KEY (`idconnect`);

--
-- Indexes for table `ksiazka`
--
ALTER TABLE `ksiazka`
  ADD PRIMARY KEY (`idksiazka`),
  ADD UNIQUE KEY `isbn` (`isbn`);

--
-- Indexes for table `paleta`
--
ALTER TABLE `paleta`
  ADD PRIMARY KEY (`idpaleta`);

--
-- Indexes for table `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`idplace`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `connect`
--
ALTER TABLE `connect`
  MODIFY `idconnect` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `ksiazka`
--
ALTER TABLE `ksiazka`
  MODIFY `idksiazka` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT dla tabeli `paleta`
--
ALTER TABLE `paleta`
  MODIFY `idpaleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT dla tabeli `place`
--
ALTER TABLE `place`
  MODIFY `idplace` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
