<?php
session_start();
	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
		require_once "connect.php";
		mysqli_report(MYSQLI_REPORT_STRICT);
		
		$polaczenie = new mysqli($host,$user,$password,$database);
		$polaczenie->set_charset("utf8");
	
		$min_isbn=$polaczenie->query("SELECT isbn FROM ksiazka WHERE isbn<1000 ORDER BY isbn DESC LIMIT 1");
	
	
	if (isset($_POST['isbn']))
	{
		$wszystko_ok=true; //założenie walidacji
		//ISBN
		$isbn=$_POST['isbn'];
		//dlugosc lancucha
		
		if((strlen($isbn)>4)&& (strlen($isbn)<10) || (strlen($isbn)>13))
		{
			$wszystko_ok=false;
			$_SESSION['e_isbn']="ISBN musi posiadać od 10 do 13 cyfr!";
		}
		//cyfry
		if (ctype_digit($isbn)==false)
		{
			$wszystko_ok=false;
			$_SESSION['e_isbn']="ISBN może się składać tylko z cyfr!";
		}
		
		//tytul
		$tytul=$_POST['tytul'];
		if((strlen($tytul)<1))
		{
			$wszystko_ok=false;
			$_SESSION['e_tytul']="Pole nie może być puste";
		}
		//autor
		
		//rok - 4 cyfry
		$rok=$_POST['rok'];

		if((strlen($rok)!=4) && ($rok!=0))
		{
			$wszystko_ok=false;
			$_SESSION['e_rok']="Podaj dokładnie rok!";
		
		/*if(($rok<'1980')||$rok>(date("Y")))
		{
			$wszystko_ok=false;
			$_SESSION['e_rok']="Nie pierdol, że to takie stare!";
		}*/
		}
		//walidacja roku - coś tu się wyjebuje
		
		$typ=$_POST['typ'];
		$autor=$_POST['autor'];
		$wydawnictwo=$_POST['wydawnictwo'];
		$zrodlo=$_POST['zrodlo'];
		$waga=$_POST['waga'];
		$cena=$_POST['cena'];
		$ilosc=$_POST['ilosc'];
		$data_mod=date("Y-n-d");
		$tworca=$_SESSION['user'];
		
		
	
		
		
		
		try 
		{
			if($polaczenie->connect_errno!=0)
		{
			throw new Exception(mysqli_connect_errno());
		}
		else
		{	
			//Sprawdzanie e-maila
			$rezultat=$polaczenie->query("SELECT idksiazka FROM ksiazka WHERE tytul='$tytul'");
			
			if(!$rezultat) 
			{
				throw new Exception($polaczenie->error);
			}
			
			$ile_tytul=$rezultat->num_rows;
			
			if($ile_tytul>0)
			{
				$wszystko_ok=false;
				$_SESSION['e_tytul']="Podany tytul już istnieje w bazie!";	
			}
			//sprawdzanie usera
			
			$rezultat=$polaczenie->query("SELECT idksiazka FROM ksiazka WHERE isbn='$isbn'");
			
			
			if(!$rezultat) 
			{
				throw new Exception($polaczenie->error);
			}
			
			$ile_isbn=$rezultat->num_rows;
			
			if($ile_isbn>0)
			{
				$wszystko_ok=false;
				$_SESSION['e_isbn']="Podany numer isbn już istnieje w bazie!";	
			}
			
			//nie wrzuca polskich znaków
			if($wszystko_ok==true)
			{
				if($polaczenie->query("INSERT INTO ksiazka VALUES (NULL, '$typ','$isbn','$tytul','$autor','$rok','$wydawnictwo','$zrodlo','$waga','$cena','$ilosc','$data_mod','$tworca')"))
				{
					$_SESSION['e_add']="Artykuł dodany pomyślnie!";		
				}
				else
				{
					throw new Exception($polaczenie->error);
				}
			}
			

			$polaczenie->close();
		}
			
		}
		catch (Exception $e)
		{
			echo 'Błąd serwera!';
			echo '<br/>Dev Info: '.$e;//zakomciać! <3 <3 <3
		}
	}
	
	
?>


<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8"/>
	<title>Lednicki system sprzedażowo-magazynowy</title>
	<meta name="description" content="Opis strony"/>
	
	<meta http-equiv="X-UA-Compatyible" content ="IE=edge,chrome=1"/>
	<link rel="stylesheet" type="text/css" href="style.css" media="all">
</head>

<body>

	<div id="container">
		<div id="header">
		<h1>Lednicki system magazynowy</h1>
	
		</div>
		<div id="formularz">
		<h3>Dodaj artykuł</h3>
		<form  method="post">
		Typ: <select name="typ">
		<option>Gadżet</option>
		<option>Książka</option>
		<option>Inne</option>
		</select>
		<br />
		ISBN: <input type="text" name="isbn" /> <br />
		<?php //echo'Ostani własny ISBN: '.$min_isbn; ?>
		<?php 
		if (isset($_SESSION['e_isbn']))
		{
			echo '<div class="error">'.$_SESSION['e_isbn'].'</div>';
			unset($_SESSION['e_isbn']);
		}
		?>
		Tytuł/Nazwa: <input type="text" name="tytul"  /><br />
		<?php 
		if (isset($_SESSION['e_tytul']))
		{
			echo '<div class="error">'.$_SESSION['e_tytul'].'</div>';
			unset($_SESSION['e_tytul']);
		}
		?>
		Autor: <input type="text" name="autor"/><br />
		Rok: <input type="text" name="rok"/><br />
		<?php
		if (isset($_SESSION['e_rok']))
		{
			echo '<div class="error">'.$_SESSION['e_rok'].'</div>';
			unset($_SESSION['e_rok']);
		}
		?>
		Wydawnictwo: <input type="text" name="wydawnictwo"/><br />
		Źródło: <input type="text" name="zrodlo"/><br />
		Waga: <input type="text" name="waga"/><br />
		Cena: <input type="text" name="cena"/><br />
		Ilość: <input type="text" name="ilosc"  /><br />
		<!--Data dodania: <input type="date" name="data_mod"/><br />
		-->
		
		<br /><button type="submit">Dodaj przedmiot</button>
		<?php if (isset($_SESSION['e_add']))
		{
			echo '<div class="error">'.$_SESSION['e_add'].'</div>';
			unset($_SESSION['e_add']);
		}
		?>
		<br /><br /><button type="reset">Wyczyść dane</button>
		</form>
		<p>
		<p>
		</div>
		
		<div id="footer">
		<h3>Prawa autorskie</h3>
		
		<div id="powrot">[<a href="main.php">Strona Główna</a>]</div>
		<p>
		[<a href="logout.php">Wyloguj się!</a>]
		</p>
		</div>
		
		
	</div>
	
</body>
</html>