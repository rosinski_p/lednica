﻿<?php

	session_start();
	
	if((!isset($_POST['login'])) || (!isset($_POST['haslo'])))
	{
		header('Location: index.php');
		exit();
	}
	
	require_once "connect.php";
	
	$polaczenie = @new mysqli($host,$user,$password,$database);
	
	if($polaczenie->connect_errno!=0)
	{
		echo"Error: ".$polaczenie->connect_errno;
	}
	else
	{
		$login = $_POST['login'];
		$haslo = $_POST['haslo'];
		
		$login=htmlentities($login, ENT_QUOTES, "UTF-8");
		
		//$sql = "SELECT * FROM users WHERE user='$login' AND password='$haslo'";
		
		if ($rezultat = $polaczenie->query(
		sprintf("SELECT * FROM users WHERE user='%s'",
		mysqli_real_escape_string($polaczenie,$login))))
		{
			$ile_user = $rezultat->num_rows;
			if($ile_user>0)
			{
				$wiersz = $rezultat->fetch_assoc();
				
				if(password_verify($haslo, $wiersz['password']))
				{
					$_SESSION['zalogowany']=true;
					
					$_SESSION['id']=$wiersz['id'];
					$_SESSION['user']= $wiersz['user'];
					//$_SESSION[''] = $wiersz['']; //wyciągnąć sobie poziom usera i jego ostatnie logowanie
				
					unset($_SESSION['blad']);
					$rezultat->close();
					header('Location: main.php');
				}
				else
				{
					$_SESSION['blad']='<span style="color:red">Nieprawidłowy login lub hasło!</span>';
					header('Location: index.php');
					
				}
				
			}
			else
			{
				$_SESSION['blad']='<span style="color:red">Nieprawidłowy login lub hasło!</span>';
				header('Location: index.php');
				
			}
			
		}
		
		$polaczenie->close();
	}
	
	
?>